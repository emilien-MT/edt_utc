package utils

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

type Post struct {
	Origin      string
	Referer     string
	ContentType string
	Body        map[string]string
	Cookie      []string
	Url         string
	Host        string
	UserAgent   string
}

type Get struct {
	Url           string
	Host          string
	UserAgent     string
	Origin        string
	Referer       string
	Cookie        []string
	Authorization string
}

type Response struct {
	Code     int
	Body     string
	Cookie   string
	Location string
	Headers  http.Header
}

func ExtractInputValue(htmlContent string, inputNameOrID string) (string, error) {
	// Parse the HTML content
	doc, err := html.Parse(strings.NewReader(htmlContent))
	if err != nil {
		return "", fmt.Errorf("failed to parse HTML: %v", err)
	}
	// Traverse the HTML node tree
	var searchInput func(*html.Node) string
	searchInput = func(n *html.Node) string {
		if n.Type == html.ElementNode && n.Data == "input" {
			var inputValue string
			var match bool
			for _, attr := range n.Attr {
				if (attr.Key == "name" || attr.Key == "id") && attr.Val == inputNameOrID {
					match = true
				}
				if attr.Key == "value" {
					inputValue = attr.Val
				}
			}
			if match {
				return inputValue
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			if val := searchInput(c); val != "" {
				return val
			}
		}
		return ""
	}

	value := searchInput(doc)
	if value == "" {
		return "", fmt.Errorf("input tag with name or id '%s' not found", inputNameOrID)
	}
	return value, nil
}

func GetRequest(get Get, auth bool) Response {
	// http Query
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	req, err := http.NewRequest("GET", get.Url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Cookie", Array2String(get.Cookie))
	req.Header.Set("Origin", get.Origin)
	req.Header.Set("Referer", get.Referer)
	req.Header.Set("Host", get.Host)
	req.Header.Set("User-Agent", get.UserAgent)
	if auth {
		req.Header.Set("Authorization", "Bearer "+get.Authorization)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	response := Response{Location: resp.Header.Get("Location"), Cookie: resp.Header.Get("Set-Cookie"), Body: "", Headers: resp.Header, Code: resp.StatusCode}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	response.Body = string(body)
	return response
}

func Map2String(m map[string]string) string {
	// map to string
	var str string
	for k, v := range m {
		str += k + "=" + v + "&"
	}
	if len(str) == 0 {
		return ""
	}
	return str[:len(str)-1]
}

func Array2String(a []string) string {
	// array to string
	var str string
	for _, v := range a {
		str += v + "; "
	}
	if len(str) == 0 {
		return ""
	}
	return str[:len(str)-2]
}

func PostRequest(post Post) Response {
	// http Post
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	req, err := http.NewRequest("POST", post.Url, strings.NewReader(Map2String(post.Body)))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", post.ContentType)
	req.Header.Set("Cookie", Array2String(post.Cookie))
	req.Header.Set("Origin", post.Origin)
	req.Header.Set("Referer", post.Referer)
	req.Header.Set("Host", post.Host)
	req.Header.Set("User-Agent", post.UserAgent)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	response := Response{Location: resp.Header.Get("Location"), Cookie: resp.Header.Get("Set-Cookie"), Body: ""}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	response.Body = string(body)
	return response
}

func ExtractToken(url string) string {
	// Extract access
	access := strings.Split(url, "#access_token=")[1]
	access = strings.Split(access, "&")[0]
	return access
}
