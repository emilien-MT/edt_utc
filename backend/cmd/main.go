package main

import (
	"edt/pkg/utils"
	"fmt"
)

func main() {
	username := "username"
	password := "password"
	cookies := []string{"org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE=fr", "DISSESSION=62727e9a-c710-4356-bea3-248818d2b52a"}
	getMainPage := utils.Get{Origin: "https://cas.utc.fr", Referer: "https://cas.utc.fr/cas/login", Url: "https://cas.utc.fr/cas/login", Cookie: cookies, Host: "cas.utc.fr", UserAgent: "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1"}
	main := utils.GetRequest(getMainPage, false)
	executionToken, err := utils.ExtractInputValue(main.Body, "execution")
	if err != nil {
		panic(err)
	}
	cookies = append(cookies, main.Cookie)
	// cookies
	authCAS := utils.Post{Origin: "https://cas.utc.fr", Referer: "https://cas.utc.fr/cas/login", Url: "https://cas.utc.fr/cas/login", Body: map[string]string{"username": username, "password": password, "execution": executionToken, "_eventId": "submit"}, Cookie: cookies, Host: "cas.utc.fr", ContentType: "application/x-www-form-urlencoded", UserAgent: "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1"}
	CAS := utils.PostRequest(authCAS)
	cookies = append(cookies, CAS.Cookie)

	token, err := getEdtToken(cookies)
	if err != nil {
		panic(err)
	}
	ics, _ := utils.GetEDT(token)
	fmt.Println(ics)
	// fmt.Println(jsonData)
}

func getEdtToken(cookies []string) (string, error) {
	getRequest := utils.Get{Origin: "", Referer: "https://ngapplis.utc.fr/", Url: "https://cas.utc.fr/cas/oidc/oidcAuthorize?client_id=piaPROD&redirect_uri=https%3A%2F%2Fngapplis.utc.fr%2Fdossieretu&response_type=id_token&scope=openid%20profile&nonce=38a5c1743ad3db50042114bcba03cfe5e1zZaBhyA&state=f40411484018135e520673ea504cba075ac8mwAd4", Cookie: cookies, Host: "cas.utc.fr", UserAgent: "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1"}
	res := utils.GetRequest(getRequest, false)
	if res.Location == "" {
		return "ERROR", fmt.Errorf("Error, empty location in first get request. Request output: %v", res)
	}
	getRequest = utils.Get{Origin: "", Referer: "https://ngapplis.utc.fr/", Url: res.Location, Cookie: cookies, Host: "cas.utc.fr", UserAgent: "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1"}
	res = utils.GetRequest(getRequest, false)

	if res.Location == "" {
		return "ERROR", fmt.Errorf("Error, empty location in second get request. Request output: %v", res)
	}

	getRequest = utils.Get{Origin: "", Referer: "https://ngapplis.utc.fr/", Url: res.Location, Cookie: cookies, Host: "cas.utc.fr", UserAgent: "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1"}
	res = utils.GetRequest(getRequest, false)

	if res.Location == "" {
		return "ERROR", fmt.Errorf("Error, empty location in third get request. Request output: %v", res)
	}
	getRequest = utils.Get{Origin: "", Referer: "https://ngapplis.utc.fr/", Url: res.Location, Cookie: cookies, Host: "cas.utc.fr", UserAgent: "Mozilla/5.0 (Windows NT 5.1; rv:5.0.1) Gecko/20100101 Firefox/5.0.1"}
	res = utils.GetRequest(getRequest, false)

	token := utils.ExtractToken(res.Location)
	return token, nil
}
