package utils

import (
	"encoding/json"
	"strings"
	"time"
)

type lesson struct {
	EdtId      int    `json:"scheduleId"`
	TypeLesson string `json:"type"`
	Groupe     string `json:"groupe"`
	Salle      string `json:"salle"`
	Uv         string `json:"uv"`
	Debut      string `json:"debut"`
	Fin        string `json:"fin"`
	Aoub       string `json:"aoub"`
	Jour       string `json:"jour"`
}

type day struct {
	SemId     int    `json:"semId"`
	DateDebut string `json:"dateDebut"`
	Jour      string `json:"jour"`
	DayType   string `json:"type"`
}

type daytime struct {
	Date   string   `json:"date"`
	Lesson []lesson `json:"lessons"`
}

type schedule struct {
	Lessons []lesson
}

type calendar struct {
	Days []day
}

// PARAMS : date1, date2 string, date is in the format YYYY-MM-DD
// RETURN : bool, true if date1 is before date2, false otherwise
func compareDate(date1 string, date2 string) bool {
	dateFormat1, _ := time.Parse("2006-01-02", date1)
	dateFormat2, _ := time.Parse("2006-01-02", date2)
	if dateFormat1.Before(dateFormat2) {
		return true
	}
	return false
}

// PARAMS : time1, time2 string, time is in the format HH:MM
// RETURN : bool, true if time1 is before time2, false otherwise
func compareTime(time1 string, time2 string) bool {
	timeFormat1, _ := time.Parse("15:04", time1)
	timeFormat2, _ := time.Parse("15:04", time2)
	if timeFormat1.Before(timeFormat2) {
		return true
	}
	return false
}

// PARAMS : the schedule and the calendar previously fetched
// RETURN : the list of daytimes, each daytime is a day with its lessons
func createEDT(schedule schedule, calendar calendar) []daytime {
	var daytimes []daytime
	for _, day := range calendar.Days {
		var lessons []lesson
		for _, course := range schedule.Lessons {
			course.Jour = getDay(course.Jour)
			if course.Jour == day.Jour && (course.Aoub == day.DayType || len(course.Aoub) == 0) {
				course.TypeLesson = getLessonType(course.TypeLesson)
				lessons = append(lessons, course)

			} else if day.DayType != "A" && day.DayType != "B" {
				specialDay := getPeriod(day.DayType)
				less := lesson{TypeLesson: specialDay, Groupe: specialDay, Salle: specialDay, Uv: specialDay, Debut: "08:00", Fin: "18:00", Aoub: specialDay, Jour: specialDay}
				lessons = append(lessons, less)
				break
			}
		}
		daytime := daytime{Date: day.DateDebut, Lesson: orderDay(lessons)}
		daytimes = append(daytimes, daytime)
	}
	return daytimes
}

// PARAMS : the token of the user to fetch the calendar
// RETURN : the calendar of the user
func getCalendar(token string) calendar {
	request := Get{Url: "https://webservices.utc.fr/api/v1/edt/getcaluniv", Host: "webservices.utc.fr", UserAgent: "Mozilla/5.0", Origin: "https://webservices.utc.fr", Referer: "https://webservices.utc.fr", Cookie: []string{}, Authorization: token}
	res := GetRequest(request, true)
	var days []day
	json.Unmarshal([]byte(res.Body), &days)
	calendar := calendar{Days: days}
	return calendar
}

// Params : The abbreviation of the day
// Return : The full name of the day
func getDay(day string) string {
	switch day {
	case "LUNDI":
		return "LUNDI"
	case "MARDI":
		return "MARDI"
	case "MERCE":
		return "MERCREDI"
	case "JEUDI":
		return "JEUDI"
	case "VENDR":
		return "VENDREDI"
	case "SAMED":
		return "SAMEDI"
	default:
		return "DIMANCHE"
	}
}

// PARAMS : the token of the user
// RETURN : the EDT of the user
func GetEDT(token string) (string, string) {
	cal := getCalendar(token)
	schedule := getEDT(token)
	edt := createEDT(schedule, cal)
	orderedEDT := orderEDT(edt)
	jsonData, _ := json.Marshal(orderedEDT)
	ics := json2ics(orderedEDT)
	return ics, string(jsonData)
}

// PARAMS : the token of the user to fetch the schedule
// RETURN : the schedule of the user
func getEDT(token string) schedule {
	request := Get{Url: "https://webservices.utc.fr/api/v1/edt/getedt", Host: "webservices.utc.fr", UserAgent: "Mozilla/5.0", Origin: "https://webservices.utc.fr", Referer: "https://webservices.utc.fr", Cookie: []string{}, Authorization: token}
	res := GetRequest(request, true)
	var lessons []lesson
	json.Unmarshal([]byte(res.Body), &lessons)
	schedule := schedule{Lessons: lessons}
	return schedule
}

// Params : The abbreviation of the lesson type
// Return : The full name of the lesson type
func getLessonType(lessonType string) string {
	switch lessonType {
	case "T":
		return "TP"
	case "D":
		return "TD"
	default:
		return "AMPHI"
	}
}

// Params : The abbreviation of the period
// Return : The full name of the period
func getPeriod(period string) string {
	switch period {
	case "MEDIA":
		return "MEDIAN"
	case "VACAN":
		return "VACANCES"
	case "FINAU":
		return "FINAUX"
	case "FERIE":
		return "FERIE"
	case "RENTR":
		return "RENTREE"
	case "SEMES":
		return "SEMESTRE"
	default:
		return period
	}
}

// PARAMS : the json string of the EDT
// RETURN : the ics string of the EDT
func json2ics(daytime []daytime) string {
	str := "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nCALSCALE:GREGORIAN\r\n" //ICS file beginning
	for _, day := range daytime {
		for _, lesson := range day.Lesson {
			str += "BEGIN:VEVENT\r\n"
			str += "STATUS:CONFIRMED\r\n"
			str += "DTSTART;TZID=Europe/Paris:" + strings.Replace(day.Date, "-", "", -1) + "T" + strings.Replace(lesson.Debut, ":", "", -1) + "00\r\n"
			str += "DTEND;TZID=Europe/Paris:" + strings.Replace(day.Date, "-", "", -1) + "T" + strings.Replace(lesson.Fin, ":", "", -1) + "00\r\n"
			str += "SUMMARY:" + lesson.Uv + " " + lesson.TypeLesson + " " + lesson.Groupe + "\r\n"
			str += "LOCATION:" + lesson.Salle + "\r\n"
			str += "END:VEVENT\r\n"
		}
	}
	return str + "END:VCALENDAR" //ICS file ending
}

// PARAMS : the list of lessons of a day
// RETURN : the list of lessons of a day ordered by time
func orderDay(lessons []lesson) []lesson {
	// Order the lessons by time, debut field
	for i := 0; i < len(lessons); i++ {
		for j := i + 1; j < len(lessons); j++ {
			if compareTime(lessons[j].Debut, lessons[i].Debut) {
				lessons[i], lessons[j] = lessons[j], lessons[i]
			}
		}
	}
	return lessons
}

// PARAMS : the list of daytimes
// RETURN : the list of daytimes ordered by date
func orderEDT(daytimes []daytime) []daytime {
	// Order the daytimes by date
	for i := 0; i < len(daytimes); i++ {
		for j := i + 1; j < len(daytimes); j++ {
			if compareDate(daytimes[j].Date, daytimes[i].Date) {
				daytimes[i], daytimes[j] = daytimes[j], daytimes[i]
			}
		}
	}
	return daytimes
}
