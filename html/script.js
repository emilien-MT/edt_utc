
// script.js

let events = [];
let currentWeekStart = new Date(); // Current week start date

document.getElementById('icsFile').addEventListener('change', handleFileSelect);
document.getElementById('prevWeek').addEventListener('click', () => changeWeek(-1));
document.getElementById('nextWeek').addEventListener('click', () => changeWeek(1));

function handleFileSelect(event) {
    const file = event.target.files[0];
    if (file) {
        const reader = new FileReader();
        reader.onload = function(e) {
            const fileContent = e.target.result;
            parseICS(fileContent);
        };
        reader.readAsText(file);
    }
}

function parseICS(content) {
    events = [];
    const lines = content.split('\n');
    let currentEvent = null;

    lines.forEach(line => {
        line = line.trim();
        if (line.startsWith('BEGIN:VEVENT')) {
            currentEvent = {};
        } else if (line.startsWith('END:VEVENT')) {
            if (currentEvent) {
                events.push(currentEvent);
                currentEvent = null;
            }
        } else if (line.startsWith('SUMMARY:')) {
            currentEvent.summary = line.replace('SUMMARY:', '');
        } else if (line.startsWith('DTSTART')) {
            currentEvent.start = parseDate(line.replace(/.*:(.*)/, '$1'));
        } else if (line.startsWith('DTEND')) {
            currentEvent.end = parseDate(line.replace(/.*:(.*)/, '$1'));
        }
    });

    currentWeekStart = getWeekStart(new Date()); // Start with current week
    displayCalendar();
}

function parseDate(dateString) {
    // Simple parsing of the date (example: 20230907T093000Z)
    const year = dateString.slice(0, 4);
    const month = dateString.slice(4, 6) - 1; // month is 0-indexed
    const day = dateString.slice(6, 8);
    const hour = dateString.slice(9, 11);
    const minute = dateString.slice(11, 13);
    
    return new Date(Date.UTC(year, month, day, hour, minute));
}

function getWeekStart(date) {
    // Get the start of the week (Monday)
    const day = date.getUTCDay();
    const diff = day === 0 ? 6 : day - 1; // Handle Sunday as the last day
    const weekStart = new Date(date);
    weekStart.setUTCDate(date.getUTCDate() - diff);
    weekStart.setUTCHours(0, 0, 0, 0);
    return weekStart;
}

function changeWeek(offset) {
    // Change the current week by the offset (e.g., -1 for previous, +1 for next)
    currentWeekStart.setUTCDate(currentWeekStart.getUTCDate() + offset * 7);
    displayCalendar();
}

function displayCalendar() {
    const calendarDiv = document.getElementById('calendar');
    calendarDiv.innerHTML = '';

    const weekEnd = new Date(currentWeekStart);
    weekEnd.setUTCDate(weekEnd.getUTCDate() + 6); // End of the week

    // Display the current week range
    document.getElementById('currentWeek').textContent = 
        `Week: ${currentWeekStart.toDateString()} - ${weekEnd.toDateString()}`;

    // Create 7-day grid for the current week
    for (let i = 0; i < 7; i++) {
        const dayDiv = document.createElement('div');
        dayDiv.classList.add('day');

        const currentDay = new Date(currentWeekStart);
        currentDay.setUTCDate(currentWeekStart.getUTCDate() + i);

        dayDiv.innerHTML = `<strong>${currentDay.toDateString()}</strong>`;
        
        // Filter events for the current day
        const dayEvents = events.filter(event => 
            event.start >= currentDay && event.start < new Date(currentDay.getTime() + 24 * 60 * 60 * 1000)
        );

        // Append events to the respective day
        dayEvents.forEach(event => {
            const eventDiv = document.createElement('div');
            eventDiv.classList.add('event');
            eventDiv.innerText = `${event.summary} (${event.start.getUTCHours()}:${event.start.getUTCMinutes()})`;
            dayDiv.appendChild(eventDiv);
        });

        calendarDiv.appendChild(dayDiv);
    }
}
